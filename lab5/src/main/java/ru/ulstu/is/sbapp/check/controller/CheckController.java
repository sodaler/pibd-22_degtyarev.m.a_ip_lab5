package ru.ulstu.is.sbapp.check.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.ulstu.is.sbapp.WebConfiguration;
import ru.ulstu.is.sbapp.check.model.CheckDto;
import ru.ulstu.is.sbapp.test.model.TestDto;

import javax.validation.Valid;

@RestController
@RequestMapping(WebConfiguration.REST_API + "/check")
public class CheckController {
    @PostMapping
    public CheckDto checkValidation(@RequestBody @Valid CheckDto checkDto) {
        return checkDto;
    }
}
