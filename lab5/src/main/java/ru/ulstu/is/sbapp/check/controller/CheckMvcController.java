package ru.ulstu.is.sbapp.check.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.worker.controller.CityDto;
import ru.ulstu.is.sbapp.worker.controller.WorkerDto;
import ru.ulstu.is.sbapp.worker.service.CityService;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

@Controller
@RequestMapping("/check")
public class CheckMvcController {
    private final WorkerService workerService;
    private final CityService cityService;

    public CheckMvcController(WorkerService workerService, CityService cityService) {
        this.workerService = workerService;
        this.cityService = cityService;
    }

    @GetMapping
    public String getWorkers(@RequestParam(value = "cityName", defaultValue = "moscow") String cityName, @RequestParam(name = "serial", defaultValue = "24") String serial,
                             Model model) {
        model.addAttribute("cities",
                cityService.findAllCities().stream()
                        .map(CityDto::new)
                        .toList());
        model.addAttribute("cityName", cityName);
        model.addAttribute("serial", serial);
        model.addAttribute("workerDto", workerService.findByNameContaining(cityName, serial).stream()
                .map(WorkerDto::new)
                .toList());
        return "check";
    }
}
