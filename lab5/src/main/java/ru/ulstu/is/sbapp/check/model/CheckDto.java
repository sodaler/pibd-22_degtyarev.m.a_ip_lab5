package ru.ulstu.is.sbapp.check.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ru.ulstu.is.sbapp.worker.controller.WorkerDto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class CheckDto {

    @NotNull(message = "Id can't be null")
    private String cityName;
    @NotBlank(message = "Name can't be null or empty")
    private String serial;

    public CheckDto(CheckDto workerDto) {
        this.cityName= workerDto.getCityId();
        this.serial= workerDto.getSerial();
    }

    public String getCityId() {
        return cityName;
    }

    public void setCityId(String cityName) {
        this.cityName = cityName;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    public String getData() {
        return String.format("%s %s", cityName, serial);
    }

    @JsonIgnore
    public String getAnotherData() {
        return "Check";
    }


}
