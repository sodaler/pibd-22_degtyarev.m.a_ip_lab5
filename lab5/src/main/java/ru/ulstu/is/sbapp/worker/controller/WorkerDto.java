package ru.ulstu.is.sbapp.worker.controller;

import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;

import javax.validation.constraints.NotBlank;

public class WorkerDto {
    private long id;
    @NotBlank
    private String firstName;
    @NotBlank
    private String lastName;
    private String number;
    private String serial;
    private Long city;
    private String cityName;

    public WorkerDto(Worker worker) {
        this.id = worker.getId();
        this.firstName= worker.getFirstName();
        this.lastName= worker.getLastName();
        this.number= worker.getPassport().getnumber();
        this.serial= worker.getPassport().getserial();
        this.city= worker.getCity().getId();
        this.cityName= worker.getCity().getName();
    }

    public WorkerDto() {
    }

    public WorkerDto(WorkerDto workerDto) {
        this.id = workerDto.getId();
        this.city= workerDto.getCity();
        this.serial= workerDto.getSerial();
        this.firstName = workerDto.getFirstName();
        this.lastName = workerDto.getLastName();
        this.number = workerDto.getNumber();
        this.cityName = workerDto.getCityName();
    }


    public long getId() {
        return id;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getNumber() {
        return number;
    }

    public String getSerial() {
        return serial;
    }

    public Long getCity() {
        return city;
    }



    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public void setCity(Long city) {
        this.city = city;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        this.cityName = cityName;
    }
}
