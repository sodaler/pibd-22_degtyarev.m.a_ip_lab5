package ru.ulstu.is.sbapp.worker.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;
import ru.ulstu.is.sbapp.worker.service.CityService;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

import javax.validation.Valid;
import java.util.List;

@Controller
@RequestMapping("/worker")
public class WorkerMvcController {
    private final WorkerService workerService;
    private final CityService cityService;

    public WorkerMvcController(WorkerService workerService, CityService cityService) {
        this.workerService = workerService;

        this.cityService = cityService;
    }

    @GetMapping
    public String getWorkers(Model model) {
        model.addAttribute("workers",
                workerService.findAllWorkers().stream()
                        .map(WorkerDto::new)
                        .toList());
        return "worker";
    }

    @GetMapping(value = {"/edit", "/edit/{id}"})
    public String editWorker(@PathVariable(required = false) Long id,
                              Model model) {
        model.addAttribute("cities",
                cityService.findAllCities().stream()
                        .map(CityDto::new)
                        .toList());
        if (id == null || id <= 0) {

            model.addAttribute("workerDto", new WorkerDto());
        } else {
            model.addAttribute("workerId", id);
            model.addAttribute("workerDto", new WorkerDto(workerService.findWorker(id)));
        }

        return "worker-edit";
    }

    @PostMapping(value = {"", "/{id}"})
    public String saveWorker(@PathVariable(required = false) Long id,
                              @ModelAttribute @Valid WorkerDto workerDto,
                              BindingResult bindingResult,
                              Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("errors", bindingResult.getAllErrors());
            return "worker-edit";
        }
        if (id == null || id <= 0) {
            workerService.addWorker(workerDto);
        } else {
            workerService.updateWorker(workerDto);
        }
        return "redirect:/worker";
    }

    @PostMapping("/delete/{id}")
    public String deleteWorker(@PathVariable Long id) {
        workerService.deleteWorker(id);
        return "redirect:/worker";
    }

//    @GetMapping("/check")
//    public String getWorkersCheck(Model model) {
//         model.addAttribute("workRequest", workerService.findByNameContaining(name, serial));
//        model.addAttribute("workers",
//                workerService.findAllWorkers().stream()
//                        .map(WorkerDto::new)
//                        .toList());
//
//        return "check";
//    }

//    @GetMapping("/check")
//    public String getWorkers(@PathVariable(required = false) Long id, @PathVariable(required = false) String serial,
//                                      Model model) {
//        model.addAttribute("cityName", id);
//        model.addAttribute("serial", serial);
//        model.addAttribute("workers", workerService.findByNameContaining(id, serial).stream()
//                .map(WorkerDto::new)
//                .toList());
//        return "check";
//    }
}

//    @RequestParam("city") Long name, @RequestParam("serial") String serial,