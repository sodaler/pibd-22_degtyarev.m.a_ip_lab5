package ru.ulstu.is.sbapp.worker.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class City {
    public void setId(Long id) {
        this.id = id;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String name;
    @OneToMany(mappedBy = "city", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Worker> workers;

    public City(){ }

    public City(String name){
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        City City = (City) o;
        return Objects.equals(id, City.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Worker> getWorkers() {
        return workers;
    }

    public void addWorker(Worker worker) {
        workers.add(worker);
    }
    public void updateWorker(Long workerId, Worker st) {
        for(Worker worker : workers){
            if(worker.getId()==st.getId()) {
                worker = st;
                break;
            }
        }
    }
    public void removeWorker(Long workerId){
        workers.remove(workerId);
    }
    public void removeAllWorkers(){
        workers.clear();
    }
}

