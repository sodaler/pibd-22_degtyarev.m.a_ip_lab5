package ru.ulstu.is.sbapp.worker.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Passport {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String number;
    private String serial;

    public Passport(){ }

    public Passport(String number, String serial){
        this.serial = serial;
        this.number = number;
    }

    public String getnumber() {
        return number;
    }

    public String getserial() {
        return serial;
    }

    public Long getId() {
        return id;
    }
    @Override
    public String toString() {
        return "Passport{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", serial='" + serial + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Passport passport = (Passport) o;
        return Objects.equals(id, passport.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}