package ru.ulstu.is.sbapp.worker.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.ulstu.is.sbapp.worker.model.City;

import java.util.List;

public interface CityRepository extends JpaRepository<City, Long> {
}
