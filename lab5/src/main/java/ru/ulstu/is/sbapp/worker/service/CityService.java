package ru.ulstu.is.sbapp.worker.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.worker.controller.CityDto;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.repository.CityRepository;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import javax.persistence.EntityNotFoundException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CityService {
    private final Logger log = LoggerFactory.getLogger(CityService.class);
    private final CityRepository cityRepository;
    private final ValidatorUtil validatorUtil;

    public CityService(CityRepository cityRepository,
                       ValidatorUtil validatorUtil) {
        this.cityRepository = cityRepository;
        this.validatorUtil = validatorUtil;
    }

    @Transactional
    public City addCity(String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City data is null or empty");
        }
        final City City = new City(name);
        validatorUtil.validate(City);
        return cityRepository.save(City);
    }

    @Transactional(readOnly = true)
    public City findCity(Long id) {
        final Optional<City> city = cityRepository.findById(id);
        return city.orElseThrow(() -> new CityNotFoundException(id));
    }

    //this
    @Transactional(readOnly = true)
    public City findCityByName(Long name) {
        List<City> cities = cityRepository.findAll();
        for (City City : cities) {
            if (City.getName().equals(name))
                return City;
        }
        throw new EntityNotFoundException(String.format("City with name [%s] is not found", name));
    }

    @Transactional(readOnly = true)
    public City findCityById(Long id) {
        List<City> cities = cityRepository.findAll();
        for (City city : cities) {
            if (city.getId().equals(id))
                return city;
        }
        throw new EntityNotFoundException(String.format("City with id [%s] is not found", id));
    }

    @Transactional(readOnly = true)
    public List<City> findAllCities() {
        return cityRepository.findAll();
    }

    @Transactional
    public City updateCity(Long id, String name) {
        if (!StringUtils.hasText(name)) {
            throw new IllegalArgumentException("City name is null or empty");
        }
        final City currentcity = findCity(id);
        currentcity.setName(name);
        validatorUtil.validate(currentcity);
        return cityRepository.save(currentcity);
    }

    public CityDto updateCity(CityDto cityDto) {
        return new CityDto(updateCity(cityDto.getId(), cityDto.getName()));
    }

    @Transactional
    public City deleteCity(Long id) {
        City currentcity = findCity(id);
        cityRepository.delete(currentcity);
        return currentcity;
    }

    @Transactional
    public void deleteAllCitiesUnsafe() {
        log.warn("Unsafe usage!");
        List<City> cities = findAllCities();
        for(City City : cities){
            if(City.getWorkers().size()>0)
                City.removeAllWorkers();
        }
        cityRepository.deleteAll();
    }

    @Transactional
    public void deleteAllCities() throws InCityFoundWorkersException {
        List<City> cities = findAllCities();
        for(City City : cities){
            if(City.getWorkers().size()>0)
                throw new InCityFoundWorkersException("в городе" + City.getName() + "имеются рабочие");
        }
        cityRepository.deleteAll();
    }
}
