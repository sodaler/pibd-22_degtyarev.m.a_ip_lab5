package ru.ulstu.is.sbapp.worker.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import ru.ulstu.is.sbapp.worker.controller.WorkerDto;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;
import ru.ulstu.is.sbapp.worker.repository.WorkerRepository;
import ru.ulstu.is.sbapp.util.validation.ValidatorUtil;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class WorkerService {
    private final WorkerRepository workerRepository;
    private final ValidatorUtil validatorUtil;
    private final CityService cityService;

    public WorkerService(WorkerRepository workerRepository,
                         ValidatorUtil validatorUtil,
                         CityService cityService) {
        this.workerRepository = workerRepository;
        this.validatorUtil = validatorUtil;
        this.cityService = cityService;
    }

    @Transactional
    public Worker addWorker(String firstName, String lastName, String number, String serial, Long ct) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName) || !StringUtils.hasText(number) || !StringUtils.hasText(serial)) {
            throw new IllegalArgumentException("Worker data is null or empty");
        }
        City city = cityService.findCityById(ct);
        Worker worker = new Worker(firstName, lastName, number, serial, city);
        city.addWorker(worker);
        validatorUtil.validate(worker);
        return workerRepository.save(worker);
    }

    public WorkerDto addWorker(WorkerDto workerDto){
        return new WorkerDto(addWorker(workerDto.getFirstName(), workerDto.getLastName(), workerDto.getNumber(), workerDto.getSerial(), workerDto.getCity()));
    }

    @Transactional(readOnly = true)
    public Worker findWorker(Long id) {
        final Optional<Worker> worker = workerRepository.findById(id);
        return worker.orElseThrow(() -> new WorkerNotFoundException(id));
    }

    @Transactional(readOnly = true)
    public List<Worker> findAllWorkers() {
        return workerRepository.findAll();
    }

    @Transactional
    public Worker updateWorker(Long id, String firstName, String lastName, String number, String serial, Long ct) {
        if (!StringUtils.hasText(firstName) || !StringUtils.hasText(lastName) || !StringUtils.hasText(number)
                || !StringUtils.hasText(serial)) {
            throw new IllegalArgumentException("Worker name is null or empty");
        }
        final Worker currentWorker = findWorker(id);
        City city = cityService.findCityById(ct);

        currentWorker.setFirstName(firstName);
        currentWorker.setLastName(lastName);
        currentWorker.setPassport(number, serial);

        if(currentWorker.getCity().getId().equals(ct)){
            currentWorker.getCity().updateWorker(id, currentWorker);
        }
        else {
            currentWorker.getCity().removeWorker(id);
            currentWorker.setCity(city);
            city.addWorker(currentWorker);
        }

        validatorUtil.validate(currentWorker);
        return workerRepository.save(currentWorker);
    }

    public WorkerDto updateWorker(WorkerDto workerDto) {
        return new WorkerDto(updateWorker(workerDto.getId(), workerDto.getFirstName(), workerDto.getLastName(), workerDto.getNumber(), workerDto.getSerial(), workerDto.getCity()));
    }

    @Transactional
    public Worker deleteWorker(Long id) {
        final Worker currentWorker = findWorker(id);
        workerRepository.delete(currentWorker);
        return currentWorker;
    }

    @Transactional
    public void deleteAllWorkers() {
        workerRepository.deleteAll();
    }

    public List<WorkerDto> findByNameContaining(String cityName, String numPassport) {
        return workerRepository.findByNameContaining(cityName, numPassport).stream()
                .map(WorkerDto::new)
                .collect(Collectors.toList());
    }
}
