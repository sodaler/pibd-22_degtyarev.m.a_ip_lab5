package ru.ulstu.is.sbapp;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import ru.ulstu.is.sbapp.worker.model.City;
import ru.ulstu.is.sbapp.worker.model.Worker;
import ru.ulstu.is.sbapp.worker.service.CityService;
import ru.ulstu.is.sbapp.worker.service.InCityFoundWorkersException;
import ru.ulstu.is.sbapp.worker.service.WorkerService;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.List;

@SpringBootTest
public class JpaWorkerTests {
    private static final Logger log = LoggerFactory.getLogger(JpaWorkerTests.class);

    @Autowired
    private WorkerService workerService;
    @Autowired
    private CityService cityService;

    @Test
    void testWorkerCreate() {
        workerService.deleteAllWorkers();
        cityService.addCity("moscow");
        final Worker worker = workerService.addWorker("Petr", "Shishkin", "2342", "234543", 1L);
        log.info(worker.toString());
        Assertions.assertNotNull(worker.getId());
    }
    @Test
    void testWorkerCreate2() {
        workerService.deleteAllWorkers();
        cityService.addCity("ulyanovsk");
        final Worker worker = workerService.addWorker("Alex", "Petrov", "4532", "545463", 2L);
        log.info(worker.toString());
        Assertions.assertNotNull(worker.getId());
    }
    @Test
    void testWorkersCreate(){
        workerService.deleteAllWorkers();
        cityService.addCity("perm");
        ArrayList<Worker> workers = new ArrayList<>();
        City ct = new City("perm");
        workers.add(new Worker("Egor", "Letov", "5555", "222222", ct));
        workers.add(new Worker("Pedro", "Livatov", "5555", "222222", ct));
        workers.add(new Worker("John", "Solncev", "5555", "222222", ct));
        workers.add(new Worker("Bob", "Svetov", "5555", "222222", ct));

        int count =0;
        for(Worker worker : workers){
            Worker st = workerService.addWorker(worker.getFirstName(), worker.getLastName(), worker.getPassport().getserial(), worker.getPassport().getnumber(), worker.getCity().getId());
            log.info(st.toString());
            if(st!=null) count++;
        }
        Assertions.assertEquals(workers.size(), count);
    }

    @Test
    void testWorkerRead() {
        workerService.deleteAllWorkers();
        cityService.addCity("kazan");
        final Worker worker = workerService.addWorker("Petr", "Ivanov", "2222", "888888", 3L);
        log.info(worker.toString());
        final Worker findWorker = workerService.findWorker(worker.getId());
        log.info(findWorker.toString());
        log.info(workerService.findAllWorkers().toString());
        Assertions.assertEquals(worker, findWorker);
    }

    @Test
    void testWorkerReadNotFound() {
        workerService.deleteAllWorkers();
        Assertions.assertThrows(EntityNotFoundException.class, () -> workerService.findWorker(-1L));
    }

    @Test
    void testWorkerReadAll() {
        workerService.deleteAllWorkers();
        cityService.addCity("saratov");
        cityService.addCity("samara");
        workerService.addWorker("Fedor", "Eliseev", "3333", "666666", 4L);
        workerService.addWorker("Makar", "Lozhenko", "3333", "666665", 5L);
        final List<Worker> workers = workerService.findAllWorkers();
        log.info(workers.toString());
        Assertions.assertEquals(workers.size(), 2);
    }

    @Test
    void testWorkerReadAllEmpty() {
        workerService.deleteAllWorkers();
        final List<Worker> workers = workerService.findAllWorkers();
        log.info(workers.toString());
        Assertions.assertEquals(workers.size(), 0);
    }

    @Test
    void testCityCreate() throws InCityFoundWorkersException {
        cityService.deleteAllCities();
        final City city = cityService.addCity("Krim");
        log.info(city.toString());
        Assertions.assertNotNull(city.getId());
    }

    @Test
    void testCityRead() throws InCityFoundWorkersException {
        cityService.deleteAllCities();
        final City city = cityService.addCity("Anapa");
        log.info(city.toString());
        final City findCity = cityService.findCity(city.getId());
        log.info(findCity.toString());
        Assertions.assertEquals(city, findCity);
    }

    @Test
    void testCityReadNotFound() throws InCityFoundWorkersException {
        cityService.deleteAllCitiesUnsafe();
        cityService.deleteAllCities();
        Assertions.assertThrows(EntityNotFoundException.class, () -> cityService.findCity(-1L));
    }

    @Test
    void testCityReadAll() throws InCityFoundWorkersException {
        cityService.deleteAllCities();
        cityService.addCity("Novosibirsk");
        cityService.addCity("Tomsk");
        final List<City> cities = cityService.findAllCities();
        log.info(cities.toString());
        Assertions.assertEquals(cities.size(), 2);
    }

    @Test
    void testCityReadAllEmpty() throws InCityFoundWorkersException {
        cityService.deleteAllCitiesUnsafe();
        cityService.deleteAllCities();
        final List<City> cities = cityService.findAllCities();
        log.info(cities.toString());
        Assertions.assertEquals(cities.size(), 0);
    }
    @Test
    void testCityReadAllEmptyException() {
        Assertions.assertThrows(InCityFoundWorkersException.class, () -> cityService.deleteAllCities());
    }
}
